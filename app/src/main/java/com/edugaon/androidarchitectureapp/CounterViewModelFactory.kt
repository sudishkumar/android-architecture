package com.edugaon.androidarchitectureapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.edugaon.androidarchitectureapp.viewModel.CounterViewModel

class CounterViewModelFactory(val counter:Int):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CounterViewModel(counter) as T
    }
}