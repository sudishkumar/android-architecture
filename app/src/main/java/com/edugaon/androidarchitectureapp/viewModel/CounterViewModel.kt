package com.edugaon.androidarchitectureapp.viewModel

import androidx.lifecycle.ViewModel

class CounterViewModel(var counter:Int):ViewModel() {

    fun increment(){
        counter +=1
    }
}