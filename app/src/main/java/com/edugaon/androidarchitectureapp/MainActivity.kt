package com.edugaon.androidarchitectureapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.edugaon.androidarchitectureapp.viewModel.CounterViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var increaseButton: Button
    private lateinit var incrementedValueTextView :TextView

    lateinit var counterViewModel: CounterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        increaseButton = findViewById(R.id.increaseButton)
        incrementedValueTextView = findViewById(R.id.incrementedValue_textView)

        //using view model
        //counterViewModel = ViewModelProvider(this).get(CounterViewModel::class.java)

        // using view model factory
        counterViewModel = ViewModelProvider(this, CounterViewModelFactory(5)).get(CounterViewModel::class.java)

        setCounter()
        increaseButton.setOnClickListener {
            counterViewModel.increment()
            setCounter()
        }
    }

    private fun setCounter(){
        incrementedValueTextView.text = counterViewModel.counter.toString()
    }
}